# Merge Request Command Line Interface
This helper fills the inputs on the "New Merge Request" page.

## Requirements
* The GIT Repo should use the SSH Schema (.git URL) 

## Installation
```
npm install -g gitlab-mrcli
```

## Usage 
* Run the `mr` command in a folder with a git repo.
* Answer the interactive questions then your gitlab provider opens in your browser with the form filled out.
* Check it then submit the form.

## Standard Commit
* If the project has a `standard-commitrc.json` the helper will try to fill out the ticket field form the branch name
 For Details see:
 https://github.com/takl95/standard-branch
 https://github.com/takl95/standard-commit

