#!/usr/bin/env node

;(async () => {
  const shell = require('shelljs')
  const open = require('open')

  if (!shell.which('git')) {
    shell.echo('Sorry, this script requires git')
    shell.exit(1)
  }

  const baseUrl = shell
    .exec('git config --get remote.origin.url', { silent: true })
    .stdout.replace(':', '/')
    .replace('.git', '/')
    .replace('\n', '')
    .replace('git@', 'https://')

  console.log('Page for the Pipelines Overview opened. Check your browser.')
  open(`${baseUrl}-/pipelines`)
})()
