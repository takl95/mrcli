#!/usr/bin/env node

;(async () => {
  try {
    const shell = require('shelljs')
    const fuzzy = require('fuzzy')
    const chalk = require('chalk')
    const open = require('open')
    const inquirer = require('inquirer')
    inquirer.registerPrompt(
      'autocomplete',
      require('inquirer-autocomplete-prompt')
    )

    if (!shell.which('git')) {
      shell.echo('Sorry, this script requires git')
      shell.exit(1)
    }

    const { cosmiconfigSync } = require('cosmiconfig')
    const explorerSync = cosmiconfigSync('standard-commit')
    inquirer.registerPrompt(
      'autocomplete',
      require('inquirer-autocomplete-prompt')
    )
    let results = explorerSync.search()
    let config = false
    if (!results) {
      console.log(
        'Add a standard-commit.json file to the project for extra features.'
      )
    } else {
      config = results.config
    }

    const lastCommit = shell
      .exec('git log -1 --pretty=%B', { silent: true })
      .stdout.replace('\n', '')
      .trim()

    const baseUrl = shell
      .exec('git config --get remote.origin.url', { silent: true })
      .stdout.replace(':', '/')
      .replace('.git', '/')
      .replace('\n', '')
      .replace('git@', 'https://')

    const currentBranch = shell
      .exec('git rev-parse --abbrev-ref HEAD', { silent: true })
      .stdout.replace(/\s+/g, '')

    let sourceBranch = currentBranch
    const targets = shell
      .exec('git branch -r', { silent: true })
      .stdout.split('\n')
      .filter((target) => target)
      .map((target) => target.trim())
      .map((target) => target.replace('origin/', ''))
      .filter((target) => !target.includes('/'))

    let targetBranch = 'master'
    let title
    let description = ''
    let wip = false
    let removeSourceBranch

    const defaultSourceBranchPrompt = await inquirer.prompt([
      {
        name: 'confirm',
        message:
          'You are on branch ' +
          chalk.yellow(sourceBranch) +
          '. \n ' +
          chalk.cyan(
            'Are you sure you want to open a Merge Request from here?'
          ),
        default: 'Yes',
      },
    ])
    if (
      !(
        defaultSourceBranchPrompt.confirm.toLowerCase() === 'yes' ||
        defaultSourceBranchPrompt.confirm.toLowerCase() === 'y'
      )
    ) {
      await getSourceBranch()
    }

    await getTargetBranch()

    let ticket = ''
    let branchParts = []
    if (config) {
      branchParts = currentBranch.split('/')
      if (branchParts.length === 3) {
        if (config.issueFirst) {
          ticket = branchParts[0]
          branchParts.shift()
        } else {
          ticket = branchParts[2]
          branchParts.pop()
        }
      }
    }

    const titleResponse = await inquirer.prompt([
      {
        name: 'title',
        message: 'Merge Request Title',
        default:
          branchParts.length === 2
            ? branchParts.join(': ')
            : sourceBranch.replace('/', ': '),
      },
    ])
    title = titleResponse.title

    const ticketResponse = await inquirer.prompt([
      {
        name: 'ticket',
        message: 'Ticket',
        default: ticket,
      },
    ])
    ticket = ticketResponse.ticket

    const skipDescription = await promptSkipDescription()
    if (!skipDescription) {
      const descriptionResponse = await inquirer.prompt([
        {
          name: 'description',
          message: 'Merge Request Description',
          default: lastCommit,
        },
      ])
      description = descriptionResponse.description.trim()
    }

    if (ticket) {
      if (!description) {
        description = ticket
      } else {
        description += '\\' + ticket
      }
    }
    await promptWip()
    await promptRemoveSourceBranch()

    if (wip) {
      title = `WIP: ${title}`
    }
    title = encodeURI(title)
    if (description.length) {
      description = encodeURI(description)
    }

    console.log("Page for the Merge Request opened. Check your browser.")
    open(
      description.length
        ? `${baseUrl}-/merge_requests/new?merge_request[source_branch]=${sourceBranch}&merge_request[target_branch]=${targetBranch}&merge_request[title]=${title}&merge_request[description]=${description}&merge_request[force_remove_source_branch]=${removeSourceBranch}`
        : `${baseUrl}-/merge_requests/new?merge_request[source_branch]=${sourceBranch}&merge_request[target_branch]=${targetBranch}&merge_request[title]=${title}merge_request[force_remove_source_branch]=${removeSourceBranch}`
    )

    // Functions

    async function promptRemoveSourceBranch() {
      const removeSourceBranchResponse = await inquirer.prompt([
        {
          name: 'confirm',
          message: 'Delete source branch when merge request is accepted',
          default: 'No',
        },
      ])
      removeSourceBranch = +(
        removeSourceBranchResponse.confirm.toLowerCase() === 'yes' ||
        removeSourceBranchResponse.confirm.toLowerCase() === 'y'
      )
    }

    async function promptWip() {
      const wipResponse = await inquirer.prompt([
        {
          name: 'confirm',
          message: 'Mark MR as "Work In Progress"?',
          default: 'No',
        },
      ])
      wip = +(
        wipResponse.confirm.toLowerCase() === 'yes' ||
        wipResponse.confirm.toLowerCase() === 'y'
      )
    }

    async function promptSkipDescription() {
      const skipDescriptionResponse = await inquirer.prompt([
        {
          name: 'confirm',
          message: 'Would you like to add a description to the MR?',
          default: 'No',
        },
      ])
      return !(
        skipDescriptionResponse.confirm.toLowerCase() === 'yes' ||
        skipDescriptionResponse.confirm.toLowerCase() === 'y'
      )
    }

    async function getSourceBranch() {
      let sourceBranchPromptResult = await promptForBranch('Source Branch')
      const { target } = sourceBranchPromptResult
      sourceBranch = target
    }

    async function getTargetBranch() {
      let targetBranchPromptResult = await promptForBranch('Target Branch')
      const { target } = targetBranchPromptResult
      targetBranch = target
    }

    function promptForBranch(message) {
      return inquirer.prompt([
        {
          type: 'autocomplete',
          name: 'target',
          message,
          pageSize: 10,
          source: (answersSoFar, input) =>
            new Promise((resolve, reject) => {
              if (input) {
                resolve(
                  fuzzy
                    .filter(
                      input,
                      targets.filter((target) => target !== sourceBranch)
                    )
                    .map((el) => el.string)
                )
              } else {
                resolve(targets.filter((target) => target !== sourceBranch))
              }
            }).catch((e) => {}),
        },
      ])
    }
  } catch (e) {
    // Deal with the fact the chain failed
  }
})()
